
import random
import time
from concurrent import futures
from multiprocessing import freeze_support

#creating random heights, weights for 1,00,000 people


def get_health_parameters(person_details):
    bmi = calculate_bmi(person_details)
    bmi_category, health_risk = get_bmi_category_and_health_risk(bmi)
    person_details['bmi'] = bmi
    person_details['bmi_category'] = bmi_category
    person_details['health_risk'] = health_risk
    return person_details

def calculate_bmi(person):
    height_mt = person['HeightCm'] / 100
    mass_kg = person['WeightKg']

    return  round(mass_kg / height_mt, 2)

def get_bmi_category_and_health_risk(bmi):

    if bmi <= 18.5:
        return 'underweight', 'malnutrition'
    elif 18.5 < bmi <= 25:
        return 'normal', 'low risk'
    elif 25 < bmi <= 30:
        return 'overweight', 'enhanced risk'
    elif 30 < bmi <= 35:
        return 'moderately obese', 'medium risk'
    elif 35 < bmi <= 40:
        return 'severely obsese', 'high risk'
    else:
        return 'very severely obese', 'very high risk'

def timer(func):
    def inner():
        start = time.time()
        func()
        end = time.time()
        print(f'Time = {round(end - start, 2)}')
    return inner

@timer
def threads():
    data = ({"Gender": "Male", "HeightCm": random.randint(100, 200), "WeightKg": random.randint(30, 100) } 
            for _ in range(1_000_000))

    with futures.ThreadPoolExecutor() as executor:
        executor.map(calculate_bmi, data)

@timer
def sequencial():
    data = ({"Gender": "Male", "HeightCm": random.randint(100, 200), "WeightKg": random.randint(30, 100) } 
            for _ in range(1_000_000))

    return list(map(calculate_bmi, data))

@timer
def process():
    data = ({"Gender": "Male", "HeightCm": random.randint(100, 200), "WeightKg": random.randint(30, 100) } 
            for _ in range(1_000_000))

    with futures.ProcessPoolExecutor() as executor:
        executor.map(calculate_bmi, data)


if __name__ == '__main__':
    #freeze_support()
    threads()
    sequencial()
    process()