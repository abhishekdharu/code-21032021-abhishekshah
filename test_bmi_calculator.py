import pytest
from bmi_calculator import calculate_bmi, get_health_parameters, get_bmi_category_and_health_risk

def test_calculate_bmi():
    data = {"Gender": "Male", "HeightCm": 160, "WeightKg": 80 }
    bmi = calculate_bmi(data)
    assert bmi == 50.00


@pytest.mark.parametrize(
    "bmi,expected_category,expected_risk",
    [
        (18.37, 'underweight', 'malnutrition'),
        (23.87, 'normal', 'low risk'),
        (29.56, 'overweight', 'enhanced risk'),
        (33.45, 'moderately obese', 'medium risk'),
        (39.67, 'severely obsese', 'high risk'),
        (43.23,  'very severely obese', 'very high risk')

    ]
)

def test_get_bmi_category_and_health_risk(bmi, expected_category, expected_risk):
    category, risk = get_bmi_category_and_health_risk(bmi)
    assert expected_category == category
    assert expected_risk == risk

def test_get_health_parameters():
    data = {"Gender": "Male", "HeightCm": 160, "WeightKg": 80 }
    data = get_health_parameters(data)
    assert data['bmi'] == 50.0
    assert data['bmi_category'] == 'very severely obese'
    assert data['health_risk'] == 'very high risk'